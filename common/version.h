/*
  Copyright (c) 2022-2024, Intel Corporation

  SPDX-License-Identifier: BSD-3-Clause
*/

/** @file version.h
    @brief defines the ISPC version
*/

#pragma once

#define ISPC_VERSION_MAJOR 1
#define ISPC_VERSION_MINOR 25
#define ISPC_VERSION_PATCH 3
#define ISPC_VERSION "1.25.3"
